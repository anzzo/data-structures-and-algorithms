package demot_4;

import java.util.Scanner;

public class Main {

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        try{
            System.out.println("Anna tunnit: ");
            int tunnit = Integer.parseInt(in.nextLine());
            if(tunnit < 0){
                in.close();
                throw new Exception("Tunnit eivät voi olla negatiiviset");
            }

            System.out.println("Anna minuutit: ");
            int minuutit = Integer.parseInt(in.nextLine());
            if(minuutit < 0 || minuutit > 60){
                in.close();
                throw new Exception("Minuutit eivät voi mennä yli 60");
            }

            System.out.println("Anna sekunnit: ");
            int sekunnit = Integer.parseInt(in.nextLine());
            if(sekunnit < 0 || sekunnit > 60) {
                in.close();
                throw new Exception("Sekunnit eivät voi mennä yli 60");
            }

            System.out.println("Anna lisättävät sekunnit: ");
            int add = Integer.parseInt(in.nextLine());
            if(add <= 1 || add >= 1000){
                in.close();
                throw new Exception("lisättävät sekunnit pitää olla väliltä 1-1000");
            }
            in.close();

            double temp = (double) add / 60 / 60;

            int wholeHours = (int) Math.round(temp);

            temp = (temp - wholeHours) * 60;

            int wholeMinutes = (int) Math.round(temp);

            temp = (temp - wholeMinutes) * 60;
            int wholeSeconds = (int) Math.round(temp);

            wholeHours = wholeHours + tunnit;
            String hours = Integer.toString(wholeHours);
            if(wholeHours < 10){
                hours = "0" + wholeHours;
            }
            wholeMinutes = wholeMinutes + minuutit;
            String minutes = Integer.toString(wholeMinutes);
            if(wholeMinutes < 10){
                minutes = "0" + wholeMinutes;
            }
            wholeSeconds = wholeSeconds + sekunnit;
            String seconds = Integer.toString(wholeSeconds);
            if(wholeSeconds < 10){
                seconds = "0" + wholeSeconds;
            }

            String ready = "Yhdistetty aika on " + hours + ":" + minutes + ":" + seconds;
            System.out.println(ready);            
        }catch(Exception e){
            System.out.println("Virhe aikojen määrittämisessä");
            System.out.println(e.getMessage());
        }
    }     
}