package classSorting;

import java.util.Random;

public class Sorter {
    private int[] arr;
    private int koko = 10;

    public Sorter(int temp){
        this.koko = temp;
        this.arr = new int[this.koko];
        this.fillArr();
    }

    private void fillArr(){
        Random rand = new Random(); 
        for (int i = 0; i < this.arr.length; i++) {
            this.arr[i] = rand.nextInt((4 * this.koko));
        }
    }

    public void printArr(){
        for(int i = 0; i < this.arr.length; i++){
            System.out.println(this.arr[i]);
        }
    }

    public void printDumbArr(){
        String temp = "";
        for (int i = 0; i < this.arr.length; i++) {
            temp += this.arr[i];
            if(i != (this.arr.length - 1)){
                temp += ", ";
            }
        }
        System.out.println(temp);
    }

    public void shiftSortArr(boolean printMidValues){
        int temp;
        for(int i = 0; i < (this.arr.length - 1); i++){            
            for(int k = (i + 1); k < this.arr.length; k++){
                if(this.arr[i] > this.arr[k]){
                    temp = this.arr[i];
                    this.arr[i] = this.arr[k];
                    this.arr[k] = temp; 
                }
            }
            if(printMidValues) this.printDumbArr();
        }
    }

    public void shellSortArr(boolean printMidValues){
        int k, valimatka, vaihto = 1;
        valimatka = this.koko / 2;
        do {
            do {
                vaihto = 0;
                for (k = 0; k < (this.koko - valimatka); k++)
                    if (this.arr[k] > this.arr[k + valimatka]) {
                        int temp = this.arr[k];
                        this.arr[k] = this.arr[k + valimatka];
                        this.arr[k + valimatka] = temp;
                        vaihto = 1;
                    }
            } while (vaihto == 1);
        } while ((valimatka /= 2) > 0);
    }
}