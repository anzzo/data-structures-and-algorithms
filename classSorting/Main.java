package classSorting;

public class Main {

    public static void main(String[] args){
        Sorter sort = new Sorter(20);
        
        long unixTime = System.currentTimeMillis();
        sort.shiftSortArr(true);
        long unixTime2 = System.currentTimeMillis();
        
        System.out.println((unixTime2 - unixTime) + "ms");
    } 
    
}