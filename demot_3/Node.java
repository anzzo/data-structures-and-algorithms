package demot_3;

public class Node {
    public int value;
    public Node left = null;
    public Node right = null;
}