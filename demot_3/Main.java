package demot_3;

public class Main {
    public static void main(String[] args){
        Node tree = new Node();
        tree.value = 8;

        add(tree, 6);
        add(tree, 3);
        add(tree, 7);
        add(tree, 14);
        add(tree, 12);
        add(tree, 17);
        add(tree, 15);
        add(tree, 22);
        add(tree, 13);
        add(tree, 11);
        add(tree, 40);
        add(tree, 5);
        add(tree, 4);
        add(tree, 1);

        System.out.println("inorder:");
        inorder(tree);
        System.out.println();

        System.out.println("preorder:");
        preorder(tree);
        System.out.println();

        System.out.println("postorder:");
        postorder(tree);
        System.out.println();
        
    }

    public static boolean add(Node tree, int value) {

        if (value == tree.value)

            return false;

        else if (value < tree.value) {

            if (tree.left == null) {

                tree.left = new Node();
                tree.left.value = value;

                return true;

            } else

                return add(tree.left, value);

        } else if (value > tree.value) {

            if (tree.right == null) {

                tree.right = new Node();
                tree.right.value = value;

                return true;

            } else

                return add(tree.right, value);

        }

        return false;

    }

    public static void preorder(Node bt) {
        if (bt == null)
            return;
        System.out.print("  " + bt.value);
        preorder(bt.left);
        preorder(bt.right);
    }

    public static void inorder(Node bt) {
        if (bt == null)
            return;

        inorder(bt.left);
        System.out.print("  " + bt.value);
        inorder(bt.right);
    }

    public static void postorder(Node bt) {
        if (bt == null)
            return;

        postorder(bt.left);
        postorder(bt.right);
        System.out.print("  " + bt.value);
    }
}