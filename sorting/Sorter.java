
package sorting;

public class Sorter {
    private Object arr = null; 

    public Sorter(int[] arri){
        this.arr = arri;
    }
    
    public Sorter(int[][] arri) {
        this.arr = arri;
    }

    public Sorter switchSort(){
        if(this.arr instanceof int[]){
            int[] temp = (int[]) this.arr;
            for (int i = 0; i < temp.length - 1; i++){
                for (int j = i+1; j < temp.length; j++){
                    if ( temp[i] > temp[j] ){ 
                        int temppi = temp[i]; 
                        temp[i] = temp[j];
                        temp[j] = temppi; 
                    }
                }
            }
            this.arr = temp;
        }
        if(this.arr instanceof int[][]){
            //TODO
        }
        return this;
    }

    public int[] get(){
        if(this.arr instanceof int[]){
            return (int[]) this.arr;
        }
        return null;
    }
}
