
package sorting;

import java.util.Random;

public class ArrayGenerator{

    private Object arr = null;

    public ArrayGenerator(){}

    public ArrayGenerator create(int size) throws Exception{
        if(size == 0){
            throw new Exception("Koko ei voi olla nolla");
        }
        this.arr = new int[size];
        return this;
    }

    public ArrayGenerator create(int size1, int size2) throws Exception{
        if(size1 == 0 || size2 == 0){
            throw new Exception("Koko ei voi olla nolla");
        }
        this.arr = new int[size1][size2];
        return this;
    }

    public ArrayGenerator fill() throws Exception{
        if(arr == null){
            throw new Exception("Arrayn täyttäminen ei onnistu ennen arrayn luontia....");
        }
        Random rand = new Random();
        if(this.arr instanceof int[]){
            int[] temp = (int[])this.arr;
            for (int i = 0; i < temp.length; i++) {
                temp[i] = rand.nextInt(1000);
            }
            this.arr = temp;
        }
        if (this.arr instanceof int[][]) {
            int[][] temp = (int[][]) this.arr;
            for (int i = 0; i < temp.length; i++) {
                for (int k = 0; k < temp[i].length; k++) {
                    temp[i][k] = rand.nextInt(1000);
                }
            }
            this.arr = temp;
        }
        
        return this;
    }

    public ArrayGenerator clear(){
        this.arr = null;
        return this;
    }

    public Object get(){
        if(arr != null){
            return arr;
        }
        return new int[1]; 
    }
}