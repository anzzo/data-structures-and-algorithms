
package sorting;

public class Main {
    public static void main(String[] args) throws Exception{

        int arr_length = 10000;

        ArrayGenerator gen = new ArrayGenerator();
        int[] arr = (int[]) gen.create(arr_length).fill().get();
        int[][] arr2 = (int[][]) gen.create(arr_length, arr_length).fill().get();
        
        for(int i = 0; i < arr.length; i++){
            System.out.print(arr[i]);
            if(i != arr.length - 1){
                System.out.print(", ");
            }
        }
        System.out.println();
        System.out.println();
        System.out.println();

        Sorter sort = new Sorter(arr);
        int[] arr_sorted = sort.switchSort().get();
        
        for (int i = 0; i < arr_sorted.length; i++) {
            System.out.print(arr_sorted[i]);
            if (i != arr_sorted.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.println();
        System.out.println();
        System.out.println();

        // for (int i = 0; i < arr2.length; i++) {
        //     for (int k = 0; k < arr2[i].length; k++) {
        //         System.out.print(arr2[i][k]);
        //         if (k != arr2[i].length - 1) {
        //             System.out.print(", ");
        //         }
        //     }
        //     System.out.print("\n");
        // }
    }
}
