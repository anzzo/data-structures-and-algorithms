package demot_1_2;

import ownStuff.List.*;

public class Main {
    public static void main(String[] args){
        /**
         * 1) Toteuta linkitetty lista, jossa ovat arvot 29, 10 ja 2008, kun jäsenen rakenne on seuraavanlainen:
         * 
         * Rakenne nyt ei ole ihan sama kuin esimerkissä, mutta eiköhän kelpaa?
         */
        List lista = new List();
        lista.add(new ListElement(29));
        lista.add(new ListElement(10));
        lista.add(new ListElement(2008));

        /**
         * 2) Kerro, miten tulostat listalta sen jäsenen arvon, jossa arvo on 10?
         * 
         * Looppaan listan läpi ja tsekkaan arvoa, jos se löytyy tulostan ja breakkaan loopin
         */

         for(ListElement apu = lista.getFirst(); apu != null; apu = apu.getNext()){
             if(apu.getValue() == 10){
                 System.out.println("Löytyi elementti arvolla 10!!!!!");
                 break;
             }
         }

         /**
          * 3) Kuinka tulostat listan alusta loppuun?
          * Käyttämällä siihe valmiiksi kirjoittamaan funktiota ;)
          * 
          * Tai jos käsin haluaa tehdä niin looppaamalla
          */
          lista.printListValues();

          for (ListElement apu = lista.getFirst(); apu != null; apu = apu.getNext()) {
              System.out.println("Käsin: " + apu.getValue());
          }

          /**
           * 4) Kuinka laitat uuden jäsenen, jossa on arvo 99, listan alkuun?
           * 
           * Looppaan listan loppuun ja lisään sinne tai käytän valmista funktiota jonka kirjoitin
           * en nyt jaksa kopioida koodia toisesta kansiosta myös tähän
           */

          lista.add(new ListElement(99));

          /**
           * 5) Kuinka laitat uuden jäsenen, jossa on arvo -15, listan loppuun?
           * sama kaava kuin edellisessä
           */
          lista.add(new ListElement(-15));

          /**
           * 6) Kuinka laitat uuden jäsenen, jossa on arvo 5000, listan keskelle arvon 10 jälkeen?
           * 
           * Käytetään hyväksi valmiiksi kirjoittamaan funktiota joka lisää indexillä. Eli tarve on vain löytää oikea indexi
           * eli etsitään kympin indexi ja lisätään yksi, jotta funktio lisää uuden arvon oikeaan paikkaan
           */
          int index = 0;
          for (ListElement apu = lista.getFirst(); apu != null; apu = apu.getNext()) {
              if (apu.getValue() == 10) {
                  //Tämä lisäys indexiin, jotta uusi arvo tulee 10 jälkeen
                  index++;
                  break;
              }
              index++;
          }
          lista.add(new ListElement(5000), index);
          lista.printListValues();

          /**
           * 7) Kuinka poistat alkion listan alusta?
           * Javalla riittää kun poistaa linkitykset ja siirtää ensimmäisen osoittamaan toiseen.
           */
          lista.deleteFirst();
          lista.printListValues();

          /**
           * 8) Kuinka poistat alkion listan lopusta?
           * Samalla tekniikalla kuin ensimmäisen
           */
          lista.deleteLast();
          lista.printListValues();
          
          /**
           * 9) Kuinka poistat alkion 10?
           * Etsin arvolla 10 olevan alkion ja poistan sen linkitykset
           */
          lista.deleteByValue(10);
          lista.printListValues();

          /**
           * 10) Selitä pino ja jono
           * Pinoon lisätään aina ensimmäiseksi eli vähän niinkuin lautaspino (Last In First Out)
           * 
           * Jonoon lisätään viimeiseksi, vähän niinkuin kaupankassalle jono (First In First Out)
           */

           /**
            * Bonus Luo 2 satunnaista polynomia ja toteuta ne linkitettyinä listoina. Laske
            * sitten polynomit yhteen ja tulosta summapolynomi.
            *
            * Vielä ei ole aikaa tehdä bonus tehtävää
            */

            //Jos haluat että piirrän kuvia, niin sekin onnistuu. Mutta ymmärsin asian ilmankuviakin
    }   
}