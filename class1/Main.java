
package class1;

import java.lang.System;

public class Main {
    public static void main(String[] args) {
        System.out.println("=====ALKU=====");
        /* Luodaan oma linkki rakenne */
        Alkio alku = new Alkio();
        alku.arvo = 10;

        Alkio temp = new Alkio();
        temp.arvo = 33;
        alku.seuraava = temp;

        Alkio loppu = temp;

        temp = new Alkio();
        temp.arvo = 7777;

        loppu.seuraava = temp;
        loppu = loppu.seuraava;

        loppu.seuraava = null;

        System.out.println("Eka tapa:");
        /* Tulostus tapa yksi */ 
        System.out.println(alku.arvo);
        System.out.println(alku.seuraava.arvo);
        System.out.println(alku.seuraava.seuraava.arvo);

        System.out.println("Toka tapa:");
        /* Tulostus tapa kaksi */ 
        for(Alkio apu = alku; apu != null; apu = apu.seuraava){
            System.out.println(apu.arvo);
        }

        /* Luodaan alkuun uusi alkio */
        temp = new Alkio();
        temp.arvo = -99;
        temp.seuraava = alku;
        alku = temp;
        System.out.println("Tulostetaan alkion lisäämisen jälkeen");
        for(Alkio apu = alku; apu != null; apu = apu.seuraava){
            System.out.println(apu.arvo);
        }

        /* Lisätään haluttuun kohtaan uusi alkio */
        Alkio edellinen = null;
        Alkio seuraava = null;
        temp = new Alkio();
        temp.arvo = 88;
        for (Alkio apu = alku; apu != null; apu = apu.seuraava) {
            if(apu.arvo == 10){
                edellinen = apu;
                seuraava  = apu.seuraava;
                break;
            }
        }
        edellinen.seuraava = temp;
        temp.seuraava = seuraava;

        System.out.println("Tulostetaan alkion lisäämisen keskelle jälkeen");
        for (Alkio apu = alku; apu != null; apu = apu.seuraava) {
            System.out.println(apu.arvo);            
        }
        System.out.println("=====LOPPU=====");
    }
}