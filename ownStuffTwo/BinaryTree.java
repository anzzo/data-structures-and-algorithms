package ownStuffTwo;

public class BinaryTree {
    public BinaryElement root;
    
    public BinaryTree(String rootName){
        this.root = new BinaryElement(rootName);
    }

    public void printTree(){
        this.root.printName();
        if (this.root.getLeftChild() != null) {
            printTree(this.root.getLeftChild());
        }
        if (this.root.getRightChild() != null) {
            printTree(this.root.getRightChild());
        }
    }

    public void printTree(BinaryElement element){
        element.printName();
        if(element.getLeftChild() != null){
            printTree(element.getLeftChild());
        }
        if(element.getRightChild() != null) {
            printTree(element.getRightChild());
        }
    }

    /* 
        Adds child in alphabet
               root
              /    \
             A      B
            / \    / \
           C   D  E   F
        and so on....
     */
    public void addChild(BinaryElement element){
        //Get right and left childs
        BinaryElement left  = this.root.getLeftChild();
        BinaryElement right = this.root.getRightChild();
        //If left child is not set
        if(left == null){
            this.root.addChild("left", element);
            return;
        }
        //If right child is not set
        if (right == null) {
            this.root.addChild("right", element);
            return;
        }
        //If both are set and left does not have children
        if(!left.isFull()){
            this.addChild(left, element);
            return;
        }
        //If both are set and right does not have children
        if (!right.isFull()) {
            this.addChild(right, element);
            return;
        }
    }

    private void addChild(BinaryElement childElement, BinaryElement element) {
        // Get right and left childs
        BinaryElement left = childElement.getLeftChild();
        BinaryElement right = childElement.getRightChild();
        // If left child is not set
        if (left == null) {
            childElement.addChild("left", element);
            return;
        }
        // If right child is not set
        if (right == null) {
            childElement.addChild("right", element);
            return;
        }
        // If both are set and left does not have children
        if (!left.isFull()) {
            this.addChild(left, element);
            return;
        }
        // If both are set and right does not have children
        if (!right.isFull()) {
            this.addChild(right, element);
            return;
        }
    }
}