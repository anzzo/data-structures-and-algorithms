package ownStuffTwo;

public class BinaryElement {
    private String name;
    private BinaryElement leftChild = null;
    private BinaryElement rightChild = null;

    public BinaryElement(String name){
        this.name = name;
    }

    public void addChild(String side, BinaryElement element){
        switch(side){
            case "left": 
                if(this.leftChild != null){
                    System.out.println("Ei voida lisätä, on jo olemassa");
                }
                this.addLeftChild(element); 
                break;
            case "right": 
                if(this.rightChild != null){
                    System.out.println("Ei voida lisätä, on jo olemassa");
                }
                this.addRightChild(element); 
                break;
            default: break;
        }
    }

    public void addLeftChild(BinaryElement element) {
        this.leftChild = element;
    }
    
    public void addRightChild(BinaryElement element) {
        this.rightChild = element;
    }

    public BinaryElement getLeftChild() {
        return this.leftChild;
    }

    public BinaryElement getRightChild() {
        return this.rightChild;
    }

    public String getName(){
        return this.name;
    }

    public void printName(){
        System.out.println(this.name);
    }

    public boolean hasChildren(){
        if(this.leftChild != null || this.rightChild != null){
            return true;
        }
        return false;
    }

    public boolean isFull() {
        if (this.leftChild != null && this.rightChild != null) {
            return true;
        }
        return false;
    }
}