package ownStuffTwo;

public class Main {
    public static void main(String[] args){
        BinaryTree puu = new BinaryTree("Root");
        // puu.printTree();
        puu.addChild(new BinaryElement("A"));
        puu.addChild(new BinaryElement("B"));
        puu.addChild(new BinaryElement("C"));
        puu.addChild(new BinaryElement("D"));
        puu.addChild(new BinaryElement("E"));
        puu.addChild(new BinaryElement("F"));
        puu.addChild(new BinaryElement("G"));
        puu.addChild(new BinaryElement("H"));
        puu.addChild(new BinaryElement("I"));
        puu.addChild(new BinaryElement("J"));
        puu.addChild(new BinaryElement("K"));
        puu.addChild(new BinaryElement("L"));
        puu.addChild(new BinaryElement("M"));
        puu.addChild(new BinaryElement("N"));
        puu.printTree();

        System.out.println("Root: " + puu.root.getName());

        //Rootin lapset
        BinaryElement leftTemp = puu.root.getLeftChild();
        BinaryElement rightTemp = puu.root.getRightChild();
        System.out.println("Root left: " + leftTemp.getName());
        System.out.println("Root right: " + rightTemp.getName());

        //"A":n lapset
        leftTemp = leftTemp.getLeftChild();
        System.out.println("A left: " + leftTemp.getName());
        leftTemp = leftTemp.getRightChild();
        System.out.println("A right: " + leftTemp.getName());
        
        // "B":n lapset
        rightTemp = rightTemp.getLeftChild();
        System.out.println("B left: " + rightTemp.getName());
        rightTemp = rightTemp.getRightChild();
        System.out.println("B right: " + leftTemp.getName());
    }
}