
package ownStuff.List;

public class ListElement {
    private int value;
    private ListElement next;
    private ListElement before;

    public ListElement() {
        //Nothing
    }

    public ListElement(int value){
        this.setValue(value);
    }

    public void setValue(int value){
        this.value = value;
    }

    public int getValue(){
        return this.value;
    }

    public void setNext(ListElement element){
        this.next = element;
    }

    public ListElement getNext(){
        return this.next;
    }

    public void setBefore(ListElement element){
        this.before = element;
    }

    public ListElement getBefore(){
        return this.before;
    }
}