
package ownStuff.List;

public class List {
    private ListElement first = null;
    private ListElement last  = null;    

    public ListElement getFirst(){
        return first;
    }

    public ListElement getLast(){
        return last;
    }

    public void printListValues(){
        System.out.println("==========LISTAN TULOSTUS ALKAA==========");
        if(this.first == null){
            System.out.println("Ei arvoja listassa");
        }
        for(ListElement ele = first; ele != null; ele = ele.getNext()){
            System.out.println(ele.getValue());
        }
        System.out.println("==========LISTAN TULOSTUS LOPPUU==========");
    }

    /**
     * Adds element to the tail of the list
     */
    public void add(ListElement element){
        /* If there is no values in list set the first */
        if(this.first == null){
            this.first = element;
            this.last  = element;
            return;
        }
        /* If there is values find the last and add after that */
        for(ListElement ele = first; ele != null; ele = ele.getNext()){
            if(ele.getNext() == null){
                ele.setNext(element);
                element.setBefore(ele);
                this.last = element;
                break;
            }
        }
    }

    /**
     * Adds element to the given index
     */
    public void add(ListElement element, int target) {
        /* If there is no values in list set the first */
        if (this.first == null) {
            this.first = element;
            this.last  = element;
            return;
        }
        /* If there is values find the last and add after that */
        int index = 0; ListElement ed = null, seur = null;
        for (ListElement ele = first; ele != null; ele = ele.getNext()) {
            if (index == target) {
                ed = ele.getBefore();
                seur = ele.getNext();
                //Asete
                ed.setNext(element);
                seur.setBefore(ele);
                element.setNext(ele);
                ele.setBefore(element);
                element.setBefore(ed);
                break;
            }
            index++;
        }
    } 
    
    /**
     * Poistaa ensimmäisen poistamalla linkitykset
     */
    public void deleteFirst(){
        this.first = this.first.getNext();
        this.first.setBefore(null);
    }

    /**
     * Poistaa viimeisen poistamalla linkitykset
     */
    public void deleteLast() {
        this.last = this.last.getBefore();
        this.last.setNext(null);
    }

    /**
     * deleteByValue
     */
    public void deleteByValue(int value){
        ListElement edel = null, seur = null;
        for (ListElement ele = first; ele != null; ele = ele.getNext()) {
            if (ele.getValue() == value) {
                //Otetaan apumuuttujiin edellinen ja seuraava
                edel = ele.getBefore();
                seur = ele.getNext();
                //Asetetaan edellisen seuraava, nykyisen seuraavaksi
                if(edel != null){
                    edel.setNext(seur);
                }else{
                    this.first = seur;
                    this.first.setBefore(null);
                }
                //Asetetaan seuraavan "edellinen", nykyisen edelliseksi
                if(seur != null){
                    seur.setBefore(edel);
                }else{
                    this.last = edel;
                    this.last.setNext(null);
                }
                break;
            }
        }
    }
}