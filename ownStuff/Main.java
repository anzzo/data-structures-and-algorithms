
package ownStuff;

import ownStuff.List.*;

public class Main{
    public static void main(String[] args){
        System.out.println("===== TESTI ALKAA =====");
        List lista = new List();

        ListElement element = null;
        for(int i = 0; i < 10; i++){
            element = new ListElement();
            element.setValue(i);
            lista.add(element);
        }

        lista.printListValues();

        ListElement vali = new ListElement();
        vali.setValue(-99);

        lista.add(vali, 4);

        vali = new ListElement();
        vali.setValue(-22);

        lista.add(vali, 8);

        lista.printListValues();
        System.out.println("===== TESTI LOPPUU =====");
    }
}