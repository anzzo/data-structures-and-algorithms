package class2;

public class Main {    
    public static void main(String[] args){
        System.out.println("=====ALKAA=====");
        //Luodaa alku ja loppu
        Alkio alku = new Alkio();
        alku.arvo = 3;
        Alkio loppu = alku;

        //Luodaan uusi jäsen
        Alkio apu = new Alkio();
        apu.arvo = 100;
        //laitetaaan alusta seuraavaksi uusi alkio
        alku.seuraava = apu;
        //Laitetaan seuraavasta edelliseksi alku
        apu.edellinen = alku;
        //Siirretään loppu muuttuja osoittamaan viimeisintä muuttujaa
        loppu = apu;

        //Luodaan uusi alkio
        apu = new Alkio();
        apu.arvo = 55;

        loppu.seuraava = apu;
        apu.edellinen  = loppu;
        loppu = apu;

        // Luodaan uusi alkio
        apu = new Alkio();
        apu.arvo = 20;

        loppu.seuraava = apu;
        apu.edellinen = loppu;
        loppu = apu;

        //Tulostetaan alusta loppuun
        System.out.println("Alusta loppuun:");
        for(Alkio ele = alku; ele != null; ele = ele.seuraava){
            System.out.println(ele.arvo);
        }

        //Tulostetaan lopusta alkuun
        System.out.println("Lopusta alkuun:");
        for (Alkio ele = loppu; ele != null; ele = ele.edellinen) {
            System.out.println(ele.arvo);
        }

        System.out.println("===Uusi alkio lisätään alkuun===");
        //Lisätään alkuun uusi alkio
        apu = new Alkio();
        apu.arvo = 99999;

        alku.edellinen = apu;
        apu.seuraava = alku;
        alku = apu;

        // Tulostetaan alusta loppuun
        System.out.println("Alusta loppuun:");
        for (Alkio ele = alku; ele != null; ele = ele.seuraava) {
            System.out.println(ele.arvo);
        }

        // Tulostetaan lopusta alkuun
        System.out.println("Lopusta alkuun:");
        for (Alkio ele = loppu; ele != null; ele = ele.edellinen) {
            System.out.println(ele.arvo);
        }

        System.out.println("===Uusi alkio lisätään keskelle===");
        // Lisätään keskelle uusi alkio
        apu = new Alkio();
        apu.arvo = 0;

        for (Alkio ele = alku; ele != null; ele = ele.seuraava) {
            if(ele.arvo == 100){
                //Asetetaan uuden alkion relaatiot
                apu.edellinen = ele;
                apu.seuraava  = ele.seuraava;
                // asetetaan vanhojen alkioiden uudet relaatiot
                ele.seuraava.edellinen = apu;
                ele.seuraava = apu;
            }
        }

        // Tulostetaan alusta loppuun
        System.out.println("Alusta loppuun:");
        for (Alkio ele = alku; ele != null; ele = ele.seuraava) {
            System.out.println(ele.arvo);
        }

        // Tulostetaan lopusta alkuun
        System.out.println("Lopusta alkuun:");
        for (Alkio ele = loppu; ele != null; ele = ele.edellinen) {
            System.out.println(ele.arvo);
        }

        System.out.println("Poistetaan alusta:");
        //Poistetaan alusta ensimmäinen
        alku = alku.seuraava;
        alku.edellinen = null;

        // Tulostetaan alusta loppuun
        System.out.println("Alusta loppuun:");
        for (Alkio ele = alku; ele != null; ele = ele.seuraava) {
            System.out.println(ele.arvo);
        }

        // Tulostetaan lopusta alkuun
        System.out.println("Lopusta alkuun:");
        for (Alkio ele = loppu; ele != null; ele = ele.edellinen) {
            System.out.println(ele.arvo);
        }

        System.out.println("=====LOPPUU=====");
    }
}